# teXisUMSA

El proyecto teXisUMSA es una colección de **plantillas**, o *templates* **LaTeX2e** en formato ```.tex``` de **manuscritos profesionales** de perfiles, tesis de grado y artículos científicos para la **UMSA** (Universidad Mayor de San Andrés) en La Paz, Bolivia.

Actualmente existen plantillas para:

* Facultad de Ciencias Puras y Naturales
  * Carrera de Informática
    * Perfil de Tesis/Proyecto de Grado ([Directorio](https://gitlab.com/amartin1911/teXisUMSA/tree/master/FCPN/informatica/perfil) | [Ejemplo](https://gitlab.com/amartin1911/teXisUMSA/blob/master/FCPN/informatica/pdf/perfil_online.pdf))
    * Tesis/Proyecto de Grado ([Directorio](https://gitlab.com/amartin1911/teXisUMSA/tree/master/FCPN/informatica/manuscrito) | [Ejemplo](https://gitlab.com/amartin1911/teXisUMSA/blob/master/FCPN/informatica/pdf/tesis_online.pdf))
* Facultad de Ingeniería
  * Carrera de Electrónica (próximamente...)

## ¿Por qué teXisUMSA?

###### Respuesta corta:
El diseño de una composición completa de un documento/manuscrito es difícil y lleva tiempo. Al utilizar una plantilla solo debes estudiar brevemente la estructura y posteriormente enfocarte en la redacción del contenido.

###### Respuesta larga:
Si bien existe un ["Manual de presentación de trabajos finales de las diferentes modalidades de graduación"](http://gaceta.umsa.bo/bitstream/umsa/166554/1/HCU-118-11.pdf) publicado por la UMSA en 2011, he observado que no se cumple ni revisa con rigurosidad el cumplimiento de los manuscritos entregados, revisados, aprobados y finalmente impresos. No solo en mi carrera, sino en otras también. Entonces, al no existir un consenso el iniciado en la tarea de redactar su manuscrito invierte innecesarias horas de trabajo en la edición de su documento.

En este sentido, teXisUMSA es mi iniciativa para uniformizar el formato de los trabajos académicos finales. Mi visión es que en un tiempo prudencial, la UMSA publique sus propias plantillas LaTeX, tal y como hacen [esta](http://web.mit.edu/thesis/tex/), [aquella](https://libguides.caltech.edu/c.php?g=512628&p=4392063) y [tantas otras](https://www.overleaf.com/gallery/tagged/thesis) universidades en el mundo.

LaTeX tiene una serie de ventajas frente a los tradicionales editores WYSIWYG (como Office Word). Éstas son:
* Creación de documentos con un acabado estético hermoso y profesional.
* Estructuramiento lógico del documento desde la concepción.
* Adecuado soporte para la composición de fórmulas matemáticas, tablas, figuras vectoriales o código fuente.
* Fácil generación de estructuras complejas, como notas al pie, refencias cruzadas, índices o bibliografías.
* Posibilidad de seguir los archivos con un sistema de control de versiones para evitar los manuscritos duplicados.

## ¿Cómo pronunciar correctamente teXisUMSA?

Primeramente, según la [guía no tan corta de LaTeX2e](https://ctan.org/tex-archive/info/lshort/spanish):
![Pronunciación LaTeX](https://pbs.twimg.com/media/DUy1Me2W4AAAKC7.jpg)

Ahora está claro como pronunciar LaTeX correctamente. Entonces, **teXisUMSA** se pronuncia **"téJsisUMSA"**. Si aún tienes problemas, revisa la pronunciación del vocablo inglés "*technology*".

## ¿Cómo usar?

#### Requerimientos

* Conocimientos **básicos** de LaTeX2e.
Existen bastantes recursos online para aprender sobre el mundo TeX. En mi opinión personal ninguna guía introductoria es mejor que [*La No Tan Corta Introducción a LaTeX*](https://ctan.org/tex-archive/info/lshort). Otra buena guía introductoria interactiva gratuita es ofrecida por [Overleaf](https://www.overleaf.com/learn/latex/Free_online_introduction_to_LaTeX_(part_1)).

* Una **distribución** completa de TeX.
[Existe una variedad de distribuciones para diferentes sistemas operativos (Windows, Mac, \*nix)](http://tex.stackexchange.com/q/55437) pero te recomiendo **TeX Live**.

###### Para Linux:

Abre una terminal, luego instala TeX Live full con:

```bash
$ sudo apt update
$ sudo apt dist-upgrade
$ sudo apt install texlive-full
```

Por favor ten en cuenta que ```texlive-full``` es un meta-paquete, así que es muy probable que se tome un tiempo para bajar todas las dependencias desde internet.

###### Para Windows

Proximamente...

#### Uso

Cualquier editor TeX de tu preferencia servirá. Mi elección personal es [TeXstudio IDE](https://www.texstudio.org/).

Para instalar TeXstudio en Linux, abre una Terminal y ejecuta:
```bash
$ sudo add-apt-repository ppa:sunderme/texstudio
$ sudo apt update
$ sudo apt install texstudio
```
Luego, identifica la plantilla adecuada. La estructura de los directorios es Facultad/Carrera/TipoDocumento.

Finalmente, abre los archivos ```.tex``` y comienza tu edición!

**Nota:** Si quieres modificar el comportamiento de la plantilla (es decir la apariencia de tu documento), busca el archivo ```estilo.sty``` ya que es allí donde se especifican los atributos de los paquetes. Si no encuentras ese archivo con extensión ```.sty``` o ```.cls``` es probable que lamentablemente todo se encuentre en la cabecera del mismo archivo ```.tex```.

## ¿Cómo contribuir?

¿Quieres añadir la plantilla para tu carrera? ¿Has encontrado errores en las plantillas actuales? ¿Quierer mejor las plantillas actuales? ¿Tienes una idea genial para el proyecto? Entonces, por favor **inicia**, o súmate, a una discusión (*issue*) [aquí](https://gitlab.com/amartin1911/teXisUMSA/issues).

## Author

* **Álvaro M. Callejas** - *Trabajo inicial* - [@amartin1911](https://gitlab.com/amartin1911)

## Licencia

Las plantillas (archivos ```.tex``` y ```.sty```) están licenciadas bajo la Licencia GNU-GPLv3 - puedes revisar el archivo [LICENSE.md](LICENSE.md) (en inglés) para más detalles.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />Por otro lado, el contenido intelectual presente en dichas plantillas está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional</a>.

## Agradecimientos

* A la comunidad [CTAN](https://ctan.org/) (The Com­pre­hen­sive TeX Archive Net­work) y el foro [TeX-LaTeX](https://tex.stackexchange.com/) en StackExchange, por su minuciosa documentación y aporte al mundo TeX.
* A los blogs [texblog](https://texblog.org/) y [Tierra de números](http://www.tierradenumeros.com), por sus consejos y trucos para la edición día a día con LaTeX.
